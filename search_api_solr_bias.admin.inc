<?php

/**
 * @file
 * Administration page callbacks for the Search API Solr Bias module.
 */

/**
 * Build search api bias admin form.
 */
function search_api_solr_bias_admin_overview($form, $form_state, SearchApiIndex $index) {
  $date_settings = variable_get('search_api_solr_bias_search_date_boost', '0:0');
  $sticky_boost = variable_get('search_api_solr_bias_search_sticky_boost', '0');
  $promote_boost = variable_get('search_api_solr_bias_search_promote_boost', '0');

  $date_options = array(
    '10:2000.0' => '10',
    '8:1000.0' => '9',
    '8:700.0' => '8',
    '8:500.0' => '7',
    '4:300.0' => '6',
    '4:200.0' => '5',
    '4:150.0' => '4',
    '2:150.0' => '3',
    '2:100.0' => '2',
    '1:100.0' => '1',
    '0:0' => t('Ignore'),
  );

  $weights = drupal_map_assoc(array(
    '21.0',
    '13.0',
    '8.0',
    '5.0',
    '3.0',
    '2.0',
    '1.0',
    '0.8',
    '0.5',
    '0.3',
    '0.2',
    '0.1',
  ));
  $weights['0'] = t('Ignore');

  $form = array();

  $form['result_bias'] = array(
    '#type' => 'fieldset',
    '#title' => t('Result biasing'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t('Give bias to certain properties when ordering the search results. Any value except <em>Ignore</em> will increase the score of the given type in search results. Choose <em>Ignore</em> to ignore any given property.'),
    '#group' => 'bias_tabs',
  );
  $form['result_bias']['search_api_solr_bias_search_sticky_boost'] = array(
    '#type' => 'select',
    '#options' => $weights,
    '#title' => t('Sticky at top of lists'),
    '#default_value' => $sticky_boost,
    '#description' => t("Select additional bias to give to nodes that are set to be 'Sticky at top of lists'."),
  );
  $form['result_bias']['search_api_solr_bias_search_promote_boost'] = array(
    '#type' => 'select',
    '#options' => $weights,
    '#title' => t('Promoted to home page'),
    '#default_value' => $promote_boost,
    '#description' => t("Select additional bias to give to nodes that are set to be 'Promoted to home page'."),
  );
  $form['result_bias']['search_api_solr_bias_search_date_boost'] = array(
    '#type' => 'select',
    '#options' => $date_options,
    '#title' => t('More recently created'),
    '#default_value' => $date_settings,
    '#description' => t('This setting will change the result scoring so that nodes created more recently may appear before those with higher keyword matching.'),
  );

  $form['type_boost'] = array(
    '#type' => 'fieldset',
    '#title' => t('Type biasing'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'bias_tabs',
  );
  $form['type_boost']['search_api_solr_bias_search_type_boosts'] = array(
    '#type' => 'item',
    '#description' => t('Specify here which node types should get a higher relevancy score in searches. Any value except <em>Ignore</em> will increase the score of the given type in search results.'),
    '#tree' => TRUE,
  );

  $type_boosts = variable_get('search_api_solr_bias_search_type_boosts', array());
  foreach (node_type_get_types() as $key => $type) {
    $form['type_boost']['search_api_solr_bias_search_type_boosts'][$key] = array(
      '#type' => 'select',
      '#title' => t('%type type content bias', array('%type' => $type->name)),
      '#options' => $weights,
      '#default_value' => !empty($type_boosts[$key]) ? $type_boosts[$key] : 0,
    );
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Search api bias admin form submit handler.
 */
function search_api_solr_bias_admin_overview_submit($form, $form_state) {

  $values = $form_state['values'];
  if (!empty($values['search_api_solr_bias_search_type_boosts'])) {
    variable_set('search_api_solr_bias_search_type_boosts', $values['search_api_solr_bias_search_type_boosts']);
  }
  if (isset($values['search_api_solr_bias_search_date_boost'])) {
    variable_set('search_api_solr_bias_search_date_boost', $values['search_api_solr_bias_search_date_boost']);
  }
  if (isset($values['search_api_solr_bias_search_sticky_boost'])) {
    variable_set('search_api_solr_bias_search_sticky_boost', $values['search_api_solr_bias_search_sticky_boost']);
  }
  if (isset($values['search_api_solr_bias_search_promote_boost'])) {
    variable_set('search_api_solr_bias_search_promote_boost', $values['search_api_solr_bias_search_promote_boost']);
  }
}
